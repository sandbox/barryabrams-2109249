$ = jQuery;

$(document).ready(function() { 
  if ($(".node-type-page-group .node-page-group").length > 0) {
    if ($("body.logged-in").length > 0) {
      addAdminTab();
    }
    createTabList();
    populateTabList();
  }
});

// create tab list
function createTabList() {
  $(".section .region-content").before("<div class='tabbed-group-menu'><ul></ul></div>");
}

// populate tab list
function populateTabList() {

  $(".field-name-page-group-sub-pages > .field-items > .field-item").each(function() {
    var title = $(this).find(".node > h2 a");
    var thisID = $(this).find(".node").attr("id");
    $(this).find(".node > h2").hide();
    $(".section .tabbed-group-menu ul").append("<li><a href='"+title.attr("href")+"' data-source='"+thisID+"'>"+title.text()+"</a></li>")
  });
  
  
  if (window.location.hash) {
    var sourceID = window.location.hash.replace('#tab-','');
    console.log(sourceID);
    $(".field-name-page-group-sub-pages #"+sourceID).addClass("active");
    var menuItem = $(".tabbed-group-menu a[data-source="+sourceID+"]");
    menuItem.addClass("active");
    $("#edit-page a").attr("href", menuItem.attr("href")+"/edit");
    
  } else {
     $(".tabbed-group-menu a").first().addClass("active");
     $(".field-name-page-group-sub-pages .node").first().addClass("active");
     $("#edit-page a").attr("href", $(".tabbed-group-menu a").attr("href")+"/edit");
  }
  
  $(".section .tabbed-group-menu ul a").click(function() {
    var sourceID = $(this).attr("data-source");
    $(".tabbed-group-menu .active").removeClass("active");
    $("#edit-page a").attr("href", $(this).attr("href")+"/edit");
    
    $(this).addClass("active");
    $(".field-name-page-group-sub-pages .node").removeClass("active");
    $(".field-name-page-group-sub-pages #"+sourceID).addClass("active");
    
    window.location.hash = "tab-"+sourceID;    
    return false;
  });
}

// add admin tab
function addAdminTab() {
  $("ul.tabs li").last().attr("id", "edit-page");
  
  var groupEdit = $("ul.tabs li").last().clone().attr("id", "");
  groupEdit.find("a").text("Edit Group");
    
  $("#edit-page").before(groupEdit);
}
